import json
from collections import defaultdict
from tqdm import tqdm
from prodi_ner_category.utils.serializer import serialize


class MakeIndex:
    def __init__(self, file):
        self.file = file

    def index(self, key):
        _type_2_label_2_index = defaultdict(set)
        i = 0
        for row in tqdm(open(self.file), total=57000000):
            i += 1

            try:
                data = json.loads(row)

            except json.decoder.JSONDecodeError:
                continue

            try:
                for label_type, labels in data[key].items():
                    for label in labels:
                        _type_2_label_2_index[label_type].add(label)
            except KeyError:
                print(data)
                continue

        type_2_label_2_index = defaultdict(dict)
        for label_type, labels in _type_2_label_2_index.items():
            if label_type not in type_2_label_2_index:
                type_2_label_2_index[label_type]['<none>'] = 0

            for i, label in enumerate(labels):
                type_2_label_2_index[label_type][label] = i + 1

        return type_2_label_2_index


if __name__ == "__main__":
    dataset = 'dataset-29'
    file = f"/mnt/data/corpus_annotated/allegro/{dataset}/corpus.json"
    idx = MakeIndex(file)
    key = 'labels'
    serialize(idx.index(key), f'indices/pickles/{dataset}/{key}_indexes.pkl')
