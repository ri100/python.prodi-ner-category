import tensorflow as tf


def point_wise_feed_forward_network(hidden_layer_dim, output_dim):
    return tf.keras.Sequential([
        tf.keras.layers.Dense(hidden_layer_dim, activation='relu', kernel_initializer='he_uniform'),  # (batch_size, seq_len, dff)
        tf.keras.layers.Dense(output_dim)  # (batch_size, seq_len, d_model)
    ])
