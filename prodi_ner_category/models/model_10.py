import tensorflow as tf
from tensorflow.keras.layers import Input, Concatenate, Conv1D, Dense, Lambda, Dropout
from tensorflow.keras import Model

from prodi_ner_category.models.layers.feed_foward_network import point_wise_feed_forward_network
from prodi_ner_category.models.layers.multi_headed_attention import MultiHeadAttention
from prodi_ner_category.models.layers.positional_encoder import positional_encoding


def model(input_shapes, output_shapes, output_labels):
    def get_short_context(input):
        bigrams = Conv1D(48, 2, padding='same', activation='elu',
                         kernel_initializer='lecun_uniform', strides=1)(input)
        threegrams = Conv1D(48, 3, padding='same', activation='elu',
                            kernel_initializer='lecun_uniform', strides=1)(input)
        fourgrams = Conv1D(48, 4, padding='same', activation='elu',
                           kernel_initializer='lecun_uniform', strides=1)(input)

        return Concatenate()([bigrams, threegrams, fourgrams])  # 16,3*48=144

    def tags_attention_head(input, mask=None):
        short_context_input = get_short_context(input)  # 16,3*48=144

        mha = MultiHeadAttention(350, num_heads=5, name="attention")(short_context_input, short_context_input,
                                                                     short_context_input, mask)
        ffn = point_wise_feed_forward_network(250, 3 * 48)(mha)
        attention_head = tf.keras.layers.LayerNormalization(epsilon=1e-6)(ffn + short_context_input)

        return Dense(96, activation="relu", kernel_initializer='he_uniform', name="latient_space")(attention_head)

    def reduce_measures(input_aux, measures_dim):
        return Dense(measures_dim)(input_aux)

    def prepare_labels_input(input_tags, tags_latent_space):
        return Concatenate()([input_tags, tags_latent_space])

    def prepare_tags_input(input, reduced_input_measures, w2v_dim):
        positions = positional_encoding(16, w2v_dim)
        return input + positions + reduced_input_measures

    def create_padding_mask(seq):  # seq: (batch_size, seq_length)
        seq = seq[:, :, -1]
        mask = tf.cast(tf.math.equal(seq, 1), tf.float32)
        return mask[:, tf.newaxis, tf.newaxis, :]

    def labeler_trainer(label_input, mask=None):
        def att(input, mask, att_dim, heads, global_dim, hld=190):
            mha = MultiHeadAttention(att_dim, num_heads=heads)(input, input, input, mask)
            ffn_1 = point_wise_feed_forward_network(hld, global_dim)(mha)

            reduced_input_1 = Dense(global_dim)(input)
            return tf.keras.layers.LayerNormalization(epsilon=1e-6)(ffn_1 + reduced_input_1)

        def product_label_head(input, name, shape, mask=None):
            context_0 = Conv1D(64, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(input)
            context_1 = Conv1D(64, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_0)
            context_2 = Conv1D(64, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_1)
            context_3 = Conv1D(64, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=2)(context_2)

            mha_1 = MultiHeadAttention(250, num_heads=5)(context_3, context_3, context_3, mask)
            mha_2 = Dense(100, activation='elu', kernel_initializer='lecun_uniform')(mha_1)
            mha_3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)(mha_2)

            context_flat = tf.keras.layers.Flatten()(mha_3)  # 5*100 = 500

            dens_1 = Dense(200, activation='elu', kernel_initializer='lecun_uniform')(context_flat)
            dens_2 = Dense(92, activation='elu', kernel_initializer='lecun_uniform')(dens_1)

            return Dense(shape[0], activation="sigmoid", name=name)(dens_2)

        def std_label_head(input, name, shape, mask=None):
            att_norm_1 = att(input, mask, att_dim=240, heads=4, global_dim=128, hld=190)

            context_0 = Conv1D(64, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(att_norm_1)
            context_1 = Conv1D(76, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_0)
            context_2 = Conv1D(64, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_1)
            context_3 = Conv1D(68, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=2)(context_2)

            context_flat = tf.keras.layers.Flatten()(context_3)
            dens_1 = Dense(200, activation='elu', kernel_initializer='lecun_uniform')(context_flat)
            dens_2 = Dense(164, activation='elu', kernel_initializer='lecun_uniform')(dens_1)
            dens_4 = Dense(96, activation='elu', kernel_initializer='lecun_uniform')(dens_2)
            return Dense(shape[0], activation="sigmoid", name=name)(dens_4)

        def small_label_head(input, name, shape, mask=None):
            att_norm_1 = att(input, mask, att_dim=120, heads=2, global_dim=76, hld=128)

            context_0 = Conv1D(64, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(att_norm_1)
            context_1 = Conv1D(60, 2, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_0)
            context_2 = Conv1D(54, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=1)(context_1)
            context_3 = Conv1D(50, 3, padding='valid',
                               activation='elu', kernel_initializer='lecun_uniform',
                               strides=2)(context_2)

            context_flat = tf.keras.layers.Flatten()(context_3)
            dens_2 = Dense(50, activation='elu', kernel_initializer='lecun_uniform')(context_flat)
            dens_4 = Dense(40, activation='elu', kernel_initializer='lecun_uniform')(dens_2)
            return Dense(shape[0], activation="sigmoid", name=name)(dens_4)

        label_2_cnn = {
            'product': product_label_head,
            'loc': std_label_head,
            'related': std_label_head,
            'context': std_label_head,
            'set': std_label_head,
            'entity': small_label_head,
            'person': std_label_head,
            'action': std_label_head,
            'body': small_label_head,
            'animal': small_label_head,
            'time': small_label_head,
            'segment': small_label_head
        }

        label_2_mask = {
            'product': None,
            'loc': mask,
            'related': mask,
            'context': mask,
            'set': mask,
            'entity': mask,
            'person': mask,
            'action': mask,
            'body': mask,
            'animal': mask,
            'time': mask,
            'segment': mask
        }

        labels_and_shapes = zip(output_labels, output_shapes[:-1])
        return [
            label_2_cnn[label_name](
                label_input,
                label_name,
                label_shape,
                label_2_mask[label_name]
            )
            for label_name, label_shape in labels_and_shapes
        ]

    w2v_dim, aux_dim = input_shapes

    input = Input(w2v_dim, name="w2vp_input")
    input_aux = Input(aux_dim, name="w2vp_aux")
    reduced_input_aux = reduce_measures(input_aux, w2v_dim[-1])  # 16, 313 <-- w2v dim

    mask = create_padding_mask(input)
    input_tags = prepare_tags_input(input, reduced_input_aux, w2v_dim[-1])
    input_tags = Dropout(.2)(input_tags)

    tags_latent_space = tags_attention_head(input_tags, mask)  # 16,96

    # stop backprop
    stopped_tags_latent_space = Lambda(lambda x: tf.stop_gradient(x), name="stop_tags")(tags_latent_space)
    stopped_input_tags = Lambda(lambda x: tf.stop_gradient(x), name="stop_input")(input_tags)

    # Labels

    labels_input = prepare_labels_input(stopped_input_tags, tags_latent_space=stopped_tags_latent_space)  # 16, 313+96
    labels_output = labeler_trainer(labels_input, mask)
    tags_output = Dense(output_shapes[-1][-1], activation="sigmoid", name="tags")(tags_latent_space)

    return Model(
        inputs=[input, input_aux],
        outputs=labels_output + [tags_output])
