import os
# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

os.environ['WANDB_MODE'] = 'dryrun'

import tensorflow as tf
import wandb
from wandb.keras import WandbCallback
from prodi_ner_category.callbacks.checkpoint import get_checkpoint
from prodi_ner_category.callbacks.reduce_lr import reduce_on_plateau
from prodi_ner_category.data.dataset import get_dataset
from prodi_ner_category.indices.labels_indices import type_2_label_2_idx
from prodi_ner_category.models.model_10 import model

# data
dataset_name = 'dataset-29'
train_file = f"/mnt/data/corpus_annotated/allegro/{dataset_name}/train.json"
eval_file = f"/mnt/data/corpus_annotated/allegro/{dataset_name}/eval.json"

# .skip((120 + 164 + 441) * 800 * 100 + 86 * 800 + 15 * 100 + 15)  # 87
train_dataset = get_dataset()
eval_dataset = get_dataset()

train_data, _ = train_dataset(train_file)
eval_data, _ = eval_dataset(eval_file)

# i = 0
# for x,y in train_data.batch(1):
#     i+=1
#     if i>5:
#         break
#     print(x)
#     print(y)
# exit()
# model
input_shape, output_shape = train_dataset.shapes
print(output_shape)
model = model(input_shape, output_shape, type_2_label_2_idx.keys())
model.load_weights("results/polar-glade-17/polar-glade-17.e-441.vl-0.0881.l-0.09200326.h5")
model.compile(loss='binary_crossentropy',
              optimizer=tf.optimizers.Adam(
                  learning_rate=6e-06,
                  beta_1=0.9, beta_2=0.98, epsilon=10e-9),
              metrics=[tf.keras.metrics.Precision(), tf.keras.metrics.Recall(), 'acc'])

# logging

wandb_project_name = "ner-category-network"
wandb_config = {
    'model_name': 'model_10',
    'train_file': train_file,
    'eval_file': eval_file,
    'batch': 800,
    'epochs': int(57000000 / (800 * 100)) * 2 * 2,
    'steps_per_epoch': 100,
    'validation_steps': 5,
    'number_of_params': model.count_params(),
    'result_path': 'results'
}

wandb.init(config=wandb_config, project=wandb_project_name, tags=None)
wandb.run.save()

# training

model.fit(
    train_data.batch(wandb_config['batch']).prefetch(3),
    validation_data=eval_data.batch(wandb_config['batch']).prefetch(1),
    steps_per_epoch=wandb_config['steps_per_epoch'],
    validation_steps=wandb_config['validation_steps'],
    epochs=wandb_config['epochs'],
    callbacks=[
        WandbCallback(),
        get_checkpoint(wandb.run.name, save_best_only=False),
        reduce_on_plateau()
    ]
)
