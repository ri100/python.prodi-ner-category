import tensorflow as tf


def reduce_on_plateau(monitor='val_loss', factor=0.95, patience=4, min_lr=0.000001):
    return tf.keras.callbacks.ReduceLROnPlateau(
        verbose=1,
        monitor=monitor, factor=factor,
        patience=patience, min_lr=min_lr)
