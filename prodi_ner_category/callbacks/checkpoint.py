import os
import tensorflow as tf

_local = os.path.dirname(__file__)


def get_checkpoint(run_name, save_best_only=True, save_weights_only=True):
    _result_folder = os.path.join(_local, "../results")
    _result_folder = f"{_result_folder}/{run_name}"
    if not os.path.isdir(_result_folder):
        os.mkdir(_result_folder)
    model_checkpoint = '%s/%s.e-{epoch:02d}.vl-{val_loss:.4f}.l-{loss:.8f}.h5' % (_result_folder, run_name)
    return tf.keras.callbacks.ModelCheckpoint(
        model_checkpoint,
        save_best_only=save_best_only,
        save_weights_only=save_weights_only,
        verbose=1)
