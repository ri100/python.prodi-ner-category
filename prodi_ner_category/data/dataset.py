from dataset_pipe.encoders.dict_to_binary_encoder import DictToBinaryEncoder
from dataset_pipe.feeds.datasets import XYDataset
from kuptoo_encoders.encoders.word_2_measure import Word2Measure
from kuptoo_encoders.encoders.word2vec_from_dict_encoder import Word2VectorFromDictEncoder
from kuptoo_encoders.encoders.word_2_tag_encoder import Word2TagEncoder

from prodi_ner_category.indices.labels_indices import type_2_label_2_idx
from prodi_ner_category.indices.ners import ner_list
from prodi_ner_category.indices.w2v_with_pos import word2vec_with_pos, word2vec_with_pos_dim


def mapper(data):
    if 'title' not in data or 'tknz' not in data['title']:
        return None

    title = data['title']['tknz']

    x = {
        'w2v': title,
        'aux': title,
    }

    ners = data['tags'] if 'tags' in data else {}
    y = {k: data['labels'][k] if k in data['labels'] else ['<none>'] for k in type_2_label_2_idx.keys()}
    y['ners'] = (title, ners)

    return x, y


def encode():
    word2measure = Word2Measure(max_chars=20, skip_if_in=word2vec_with_pos)

    x = {
        'w2v': Word2VectorFromDictEncoder(16, word2vec_with_pos, word2vec_with_pos_dim, mark_pad=True),
        'aux': Word2VectorFromDictEncoder(16, word2measure, word2measure.dim, mark_pad=True),
    }

    y = {category_group_name: DictToBinaryEncoder(type_2_label_2_idx[category_group_name])
         for category_group_name in type_2_label_2_idx.keys()}
    y['ners'] = Word2TagEncoder(16, ner_list)

    return x, y


def encode_x():
    word2measure = Word2Measure(max_chars=20, skip_if_in=word2vec_with_pos)

    x = {
        'w2v': Word2VectorFromDictEncoder(16, word2vec_with_pos, word2vec_with_pos_dim, mark_pad=True),
        'aux': Word2VectorFromDictEncoder(16, word2measure, word2measure.dim, mark_pad=True),
    }

    return x


def get_dataset():
    dataset = XYDataset("json")
    dataset.map(mapper)
    x, y = encode()
    dataset.encode(x, y)
    return dataset
