import os
import numpy as np
from prodi_ner_category.utils.serializer import unserialize

pickle_folder = os.path.dirname(__file__)
word2vec_file = os.path.join(pickle_folder, 'pickles/global/pos_alle_w2v.pickle')
word2vec_with_pos = unserialize(word2vec_file)
word2vec_with_pos_dim = 312


class Word2VecPos:

    def __init__(self):
        self.dim = word2vec_with_pos_dim

    def __getitem__(self, item):
        return np.concatenate([word2vec_with_pos[item], np.zeros(1)])

    def __contains__(self, item):
        return item in word2vec_with_pos
