import os
from prodi_ner_category.utils.serializer import unserialize

local_dir = os.path.dirname(__file__)
type_2_label_2_idx = unserialize(os.path.join(local_dir, 'pickles/dataset-29/labels_indexes.pkl'))
print(type_2_label_2_idx.keys())
