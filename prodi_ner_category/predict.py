from collections import defaultdict
from pprint import pprint

import tensorflow as tf
from dataset_pipe.encoders.dict_to_binary_encoder import DictToBinaryEncoder
from dataset_pipe.feeds.datasets import XDataset
from kuptoo_encoders.encoders.word_2_tag_encoder import Word2TagEncoder

from prodi_ner_category.data.dataset import encode_x
from prodi_ner_category.indices.labels_indices import type_2_label_2_idx
from prodi_ner_category.indices.ners import ner_list
from prodi_ner_category.models.model_10 import model


def get_dataset_x():
    dataset = XDataset()
    dataset.map(None)
    dataset.encode(encode_x())
    return dataset


# data
dataset_name = 'dataset-29'
test_file = f"/mnt/data/corpus_annotated/allegro/{dataset_name}/eval.json"

test_content = [
    "ciepła czapka uszatka",
    "corsair wireless rgb headset se",
    "poziome urz\u0105dzenie pakuj\u0105ce flow-pack fpm-010 x s/d",
    "bateria bidetowa stoj\u0105ca 7lat gwarancji czarny",
    "lampa led obrysowa 3 funkcje prawa ld518 p",
    "16/18 alfa romeo 156 kombi boczek drzwi p ty\u0142",
    "#-cz\u0119\u015bciowy zacisk drutu",
    "4f m\u0119ska kurtka softshell sportowy sfm002% xl",
]
test_dataset = get_dataset_x()

test_data, input_shape = test_dataset(test_content)
# print(shapes)
#
# i = 0
# for x in test_data:
#     i+=1
#     if i>5:
#         break
#     print(x)
# exit()
# model

output_shape = ((5708,), (231,), (480,), (59,), (7,), (34,), (13,), (147,), (12,), (10,), (37,), (4,), (16, 88))
model = model(input_shape, output_shape, type_2_label_2_idx.keys())
model.load_weights("results/warm-wave-18/warm-wave-18.e-516.vl-0.0873.l-0.09115814.h5")
model.compile(loss='binary_crossentropy',
              optimizer=tf.optimizers.Adam(
                  learning_rate=6e-06,
                  beta_1=0.9, beta_2=0.98, epsilon=10e-9),
              metrics=[tf.keras.metrics.Precision(), tf.keras.metrics.Recall(), 'acc'])

pred_y = model.predict(test_data)

output_labels = ['product', 'related', 'context', 'set', 'entity', 'segment', 'person', 'action', 'body', 'animal',
                 'loc', 'time']

decoded_output = defaultdict(dict)
for position, label in enumerate(output_labels):
    encoder = DictToBinaryEncoder(type_2_label_2_idx[label])
    for sentence, x in enumerate(pred_y[position]):
        sentence = test_content[sentence]
        decoded_output[sentence][label] = encoder.decode(x, 0.5)

ner_decoder = Word2TagEncoder(16, ner_list)

for sentence, x in enumerate(pred_y[-1]):
    sentence = test_content[sentence]
    ners = ner_decoder.decode([x])[0]
    words = sentence.split()
    ner_dict = []
    for word_position, word in enumerate(words):
        ner_label, ner_prob = ners[word_position][0]
        ner_dict.append((word, ner_label, ner_prob))
    decoded_output[sentence]['ner'] = ner_dict

pprint(decoded_output)
