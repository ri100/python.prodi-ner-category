import tensorflow as tf
import numpy as np


def recall():
    def _recall(y_true, y_pred):
        print(y_pred)
        print(y_true)
        # sample_weight = np.ones(y_true.shape[-1])
        # sample_weight[0] = 0
        r = tf.keras.metrics.Recall()
        r.update_state(y_true, y_pred)
        print(r.result())
        return r.result()
    return _recall
